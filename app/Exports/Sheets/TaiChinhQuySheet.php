<?php

namespace App\Exports\Sheets;

use Modules\Report\Entities\Aereport;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;

class TaiChinhQuySheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize
{
    protected $hocvien;
    protected $quy;
    protected $nam;

    function __construct($hocvien, $quy, $nam)
    {
        $this->hocvien = $hocvien;
        $this->quy = $quy;
        $this->nam = $nam;
    }


    /**
     * @return Builder
     */
    public function getContent($hoc_vien_id, $months)
    {
        return DB::table('thu_chi_tai_chinhs')
            ->Leftjoin('tai_chinhs', 'thu_chi_tai_chinhs.tai_chinh_id', '=', 'tai_chinhs.id')
            ->Leftjoin('dien_giai_tai_chinhs', 'thu_chi_tai_chinhs.dien_giai_tai_chinh_id', '=',
                'dien_giai_tai_chinhs.id')
            ->select(
                'tai_chinhs.thang',
                'dien_giai_tai_chinhs.ten_dien_giai',
                'thu_chi_tai_chinhs.thu',
                'thu_chi_tai_chinhs.chi',
                'tai_chinhs.tong_ngay_co_mat',
                'tai_chinhs.tong_ngay_ngi'
            )
            ->where('tai_chinhs.hoc_vien_id', $hoc_vien_id)
            ->where('tai_chinhs.thang', $months)
            ->whereYear('thu_chi_tai_chinhs.created_at', '=', $this->nam)
            ->get()->toArray();

    }

    public function collection()
    {
        switch ($this->quy) {
            case '1':
                for ($months = 1; $months <= 3; $months++) {
                    $tmpArr[] = $this->getContent($this->hocvien->id, $months);
                }
                $results = executeData($tmpArr);
                break;
            case '2':
                for ($months = 4; $months <= 6; $months++) {
                    $tmpArr[] = $this->getContent($this->hocvien->id, $months);
                }
                $results = executeData($tmpArr);
                break;
            case '3':
                for ($months = 7; $months <= 9; $months++) {
                    $tmpArr[] = $this->getContent($this->hocvien->id, $months);
                }
                $results = executeData($tmpArr);
                break;
            case '4':
                for ($months = 10; $months <= 12; $months++) {
                    $tmpArr[] = $this->getContent($this->hocvien->id, $months);
                }
                $results = executeData($tmpArr);
                break;

            default:
                $results = collect([]);
        }

        return collect($results);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Quý ' . $this->quy;
    }

    public function headings(): array
    {
        return [
            'Tháng',
            'Tên diễn giải tài chính',
            'Thu',
            'Chi',
            'Tổng ngày có mặt',
            'Tổng ngày nghỉ',
        ];
    }

}
