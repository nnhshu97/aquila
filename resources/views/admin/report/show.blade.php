@extends('admin.layouts.index')
@section('content')
    @php
        if (isset($report->nguoibaoho_id)) {
            $ten_hoc_vien = \App\Models\HocVien::where(['nguoi_bao_ho_id' => $report->nguoibaoho_id ])->first()->ten;
        }
    @endphp
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>
                        Khiếu nại của gia đình học viên
                        @php
                            if (isset($ten_hoc_vien)) {
                                echo $ten_hoc_vien;
                            }
                        @endphp
                    </h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"> Khiếu nại</li>
                    </ol>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content-header -->
    <section class="content">
        <div class="card">
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="activity">
                        <!-- Post -->
                        <div class="post">
                            <div class="user-block">
                                <span>
                                    Người bảo hộ: {{ $report->nguoibaoho->ten }}
                                </span>
                                <br>
                                <span>
                                    Học viên:
                                    @if($ten_hoc_vien != null)
                                        {{ $ten_hoc_vien }}
                                    @endif
                                </span>
                                <br>
                                <i class="fas fa-clock"></i>
                                <span>
                                    {{ date('H:i d-m-Y', strtotime($report->created_at)) }}
                                </span>
                                <br>
                                <span>
                                    Vấn đề:
                                    @foreach(config('admin.category_report') as $key => $value)
                                        @if($report->category_report == $value['val'])
                                            {{ $value['text']}}
                                        @endif
                                    @endforeach
                                </span>
                                <br>
                                <span>Nội dung :</span>
                                <p>{{ $report->content }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

