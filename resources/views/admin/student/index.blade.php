@extends('admin.layouts.index')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1> Danh sách học viên</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Danh sách học viên</li>
                    </ol>
                </div>
                <div class="col-md-6 mt-2">
                    <a href="{{ route('admin.hoc-vien.create') }}" class="btn btn-success btn-block-sm">
                        <i class="fas fa-user-plus mr-1"></i> Thêm học viên
                    </a>
                </div>
                <div class="col-md-6 mt-2 text-right">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown show">
                            <a class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" href="#" aria-expanded="true">
                                Chọn tác vụ với file excel
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <a href="{{ route('admin.hoc-vien.importView') }}" class="dropdown-item">
                                    <i class="fas fa-file-import mr-1"></i> Nhập dữ liệu
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('admin.hoc-vien.export') }}" class="dropdown-item">
                                    <i class="fas fa-file-export mr-1"></i> Xuất dữ liệu
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="{{ route('admin.hoc-vien-tot-nghiep.export') }}" class="dropdown-item">
                                    <i class="fas fa-file-export mr-1"></i> Xuất học viên đã tốt nghiệp
                                </a>
                            </div>
                        </li>
                    </ul>
                    
                </div>
                </form>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive-sm">
                                <table id="hoc_vien" class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã học viên</th>
                                        <th>Ảnh học viên</th>
                                        <th>Tên học viên</th>
                                        <th>Trạng thái học tập</th>
                                        <th>Giới tính</th>
                                        <th>Tác vụ</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::open(['route' => ['admin.hoc-vien.destroy', 'ID_REPLY_IN_URL'], 'id' => 'delete-class-form', 'class' => 'form-horizontal']) }}
        {{ method_field('DELETE') }}
        {{ Form::close() }}
    </section>

    <div class="mess-data"
         data-url-datatable="{{ route('admin.hoc-vien.list') }}"
         data-url-show="{{ route('admin.hoc-vien.show', 'ID_REPLY_IN_URL') }}"
         data-url-edit="{{ route('admin.hoc-vien.edit', 'ID_REPLY_IN_URL') }}"
         data-url-delete="{{ route('admin.hoc-vien.destroy', 'ID_REPLY_IN_URL') }}"
         data-url-taichinh="{{route('admin.taichinh.create', 'ID_REPLY_IN_URL') }}"
         data-url-theodoiyte="{{route('admin.hoc-vien.theo-doi-y-te', 'ID_REPLY_IN_URL') }}"
         data-url-vephep="{{route('admin.hoc-vien.ve-phep', 'ID_REPLY_IN_URL') }}"
         data-url-anhquatrinh="{{route('admin.hoc-vien.anhquatrinh.index', 'ID_REPLY_IN_URL') }}"
    >
    </div>

    @component('admin.components.modals.alert-delete')
        @slot('headerText')
            {{ "Bạn có muốn xóa học viên" }}
        @endslot
    @endcomponent

@endsection
@section('scripts')
    <script type="text/javascript">
        let admin = {!! json_encode(Auth::guard('admin')->user()->role) !!};

        $(function () {
            let elementData = $('.mess-data');
            let urlDataTable = elementData.data('url-datatable');
            let urlEdit = elementData.data('url-edit');
            let urlShow = elementData.data('url-show');
            let urlDelete = elementData.data('url-delete');
            let url_update = elementData.data('url-taichinh');
            let urlTheoDoiYTe = elementData.data('url-theodoiyte');
            let urlVePhep = elementData.data('url-vephep');
            let urlAnhQuaTrinh = elementData.data('url-anhquatrinh');
            let dataAdmin = $('.mess-role').data('role-admin');
            $('#hoc_vien').DataTable({
                "ajax": urlDataTable,
                "language": {
                    "url": "/admin_assets/plugins/datatables-bs4/lang/vietnamese-lang.json"
                },
                "columns": [
                    {"data": "id"},
                    {"data": "ma_hoc_vien"},
                    {"data": null},
                    {"data": "ten"},
                    { 
                        data: "trang_thai_hoc_tap", 
                        render: function(data) {
                            return `<span class="badge badge-${data.id === 1 ? 'primary' : 'success'}">${data.ten}</span>`;
                        }
                    },
                    {"data": "gioi_tinh"},
                    {"data": null},
                ],
                'columnDefs': [{
                        'targets': 0,
                        'class': "text-center align-middle"
                    }, {
                        'targets': 1,
                        'class': "text-center align-middle"
                    },
                    {
                        'targets': 2,
                        'sortable': false,
                        'class': "text-center align-middle"
                    },
                    {
                        'targets': 3,
                        'class': "text-center align-middle"
                    },
                    {
                        'targets': 4,
                        'class': "text-center align-middle"
                    },
                    {
                        'targets': 5,
                        'class': "text-center align-middle"
                    },
                    {
                        'targets': 6,
                        'sortable': false,
                        'class': "text-center align-middle"
                    }
                ],
                "paging": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "createdRow": function (row, data, index) {
                    let editLink = urlEdit.replace('ID_REPLY_IN_URL', data.id);
                    let detailLink = urlShow.replace('ID_REPLY_IN_URL', data.id);
                    let showTheoDoiYTe = urlTheoDoiYTe.replace('ID_REPLY_IN_URL', data.id);
                    let showVePhep = urlVePhep.replace('ID_REPLY_IN_URL', data.id);
                    let taichinhLink = url_update.replace('ID_REPLY_IN_URL', `hoc_vien_id=${data.id}`);
                    let anhQuaTrinh = urlAnhQuaTrinh.replace('ID_REPLY_IN_URL', data.id);
                    let dir_path = "";
                    if (data.avatar_url == null) {
                        dir_path = window.location.origin + "/admin_assets/images/logo/logo-aq-h.png";
                    } else {
                        dir_path = window.location.origin + "/storage/uploads/user_avatar/icon128/" + data.avatar_url;
                    }
                    
                    $('td', row).eq(2).empty().append(`
                        <img src="${dir_path}" class="profile-user-img img-circle" alt="${data.ten}">
                    `);
                    if (dataAdmin == 1) {
                        $('td', row).eq(6).empty().append(`
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown show">
                                    <a class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" href="#" aria-expanded="true">
                                        Tác vụ
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                        <div class="dropdown-divider"></div>
                                        <a href="${editLink}" title="Cập nhật" class="dropdown-item">
                                            <i class="fas fa-edit"></i> Cập nhật thông tin
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${anhQuaTrinh}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="far fa-images"></i> Thêm ảnh học viên tại trung tâm
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${detailLink}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-info-circle"></i> Xem chi tiết
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${taichinhLink}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-money-check-alt"></i> Thêm tài chính
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${showTheoDoiYTe}" target="_blank" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-notes-medical"></i> Theo dõi y tế
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${showVePhep}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-clipboard"></i> Về phép / Trốn về
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a type="submit" title="Xoá" class="dropdown-item delete-trigger" data-id="${data.id}">
                                            <i class="fas fa-trash"></i> Xoá học viên
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        `);
                    }
                    else if (dataAdmin == 3) {
                        $('td', row).eq(6).empty().append(`
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown show">
                                    <a class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" href="#" aria-expanded="true">
                                        Tác vụ
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                        <div class="dropdown-divider"></div>
                                        <a href="${detailLink}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-info-circle"></i> Xem chi tiết
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${taichinhLink}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-money-check-alt"></i> Thêm tài chính
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        `);
                    }else {
                        $('td', row).eq(6).empty().append(`
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item dropdown show">
                                    <a class="btn btn-info dropdown-toggle dropdown-icon" data-toggle="dropdown" href="#" aria-expanded="true">
                                        Tác vụ
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                        <div class="dropdown-divider"></div>
                                        <a href="${editLink}" title="Cập nhật" class="dropdown-item">
                                            <i class="fas fa-edit"></i> Cập nhật thông tin
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${anhQuaTrinh}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="far fa-images"></i> Thêm ảnh học viên tại trung tâm
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${detailLink}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-info-circle"></i> Xem chi tiết
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${showTheoDoiYTe}" target="_blank" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-notes-medical"></i> Theo dõi y tế
                                        </a>
                                        <div class="dropdown-divider"></div>
                                        <a href="${showVePhep}" title="Xem chi tiết" class="dropdown-item">
                                            <i class="fas fa-clipboard"></i> Về phép / Trốn về
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        `);
                    }
                },
            });

            $('body').on('click', '.delete-trigger', function (event) {
                event.preventDefault();
                $('#modal-danger').modal('show', {backdrop: 'true'});
                let action = urlDelete.replace('ID_REPLY_IN_URL', $(this).data('id'));
                $('#delete-class-form').attr('action', action);
            })

            $('body').on('click', '#modal-danger .yes-confirm', e => {
                $('#delete-class-form').submit();
            });

            function appendMes(messages, classEl) {
                let htmlCode = '';
                $.each(messages, (key1, val1) => {
                    htmlCode += `<strong>${val1}</strong><br>`
                });
                $('.text-danger.' + classEl).html(htmlCode);
            }

            function showMessErrForm(errors) {
                $.each(errors, (key, val) => {
                    switch (true) {
                        case key == 'ma_lop_hoc':
                            appendMes(val, 'ma_lop_hoc')
                            break
                        case key == 'ngay_bat_dau':
                            appendMes(val, 'ngay_bat_dau')
                            break
                        case key == 'ngay_ket_thuc':
                            appendMes(val, 'ngay_ket_thuc')
                            break
                        case key == 'ghi_chu':
                            appendMes(val, 'ghi_chu')
                            break
                    }
                })
            }

        });

    </script>
@endsection
