<?php

namespace App\Exports\Sheets;

use Modules\Report\Entities\Aereport;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use DB;
use App\Models\TaiChinh;

class TaiChinhNamSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize
{
    protected $hocvien;
    protected $nam;

    function __construct($hocvien, $nam)
    {
        $this->hocvien = $hocvien;
        $this->nam = $nam;
    }

    /**
     * @return Builder
     */
    public function getContent($hoc_vien_id, $months, $year)
    {
//        DB::enableQueryLog();
        return DB::table('thu_chi_tai_chinhs')
            ->Leftjoin('tai_chinhs', 'thu_chi_tai_chinhs.tai_chinh_id', '=', 'tai_chinhs.id')
            ->Leftjoin('dien_giai_tai_chinhs', 'thu_chi_tai_chinhs.dien_giai_tai_chinh_id', '=',
                'dien_giai_tai_chinhs.id')
            ->select(
                'tai_chinhs.thang',
                'dien_giai_tai_chinhs.ten_dien_giai',
                'thu_chi_tai_chinhs.thu',
                'thu_chi_tai_chinhs.chi',
                'tai_chinhs.tong_ngay_co_mat',
                'tai_chinhs.tong_ngay_ngi'
            )
            ->where('tai_chinhs.hoc_vien_id', $hoc_vien_id)
            ->where('tai_chinhs.thang', $months)
            ->whereYear('thu_chi_tai_chinhs.created_at', '=', $year)
            ->get()->toArray();
//        dd(DB::getQueryLog());
    }


    public function collection()
    {

        for ($months = 1; $months <= 12; $months++) {
            $tmpArr2[] = $this->getContent($this->hocvien->id, $months, $this->nam);
        }

        $results = executeData($tmpArr2);
        return collect($results);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Năm ' . (string)$this->nam;
    }

    public function headings(): array
    {
        return [
            'Tháng',
            'Tên diễn giải tài chính',
            'Thu',
            'Chi',
            'Tổng ngày có mặt',
            'Tổng ngày nghỉ',
        ];
    }

}
