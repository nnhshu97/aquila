<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Admin\TinhTrangNghienService;
use App\Models\HocVien;
use DB;
class TinhTrangNghienController extends Controller
{
    protected $TinhTrangNghienService;

    public function __construct(TinhTrangNghienService $TinhTrangNghienService)
    {
        $this->TinhTrangNghienService = $TinhTrangNghienService;
    }

    /**
     * Display a listing of the resource.
     *m
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.tinhtrangnghien.index');        
       
    }

    public function list(Request $request)
    {
        $listhocvien = $this->TinhTrangNghienService->getListTinhTrangNghien($request->all());
        return $listhocvien;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $hocvien = HocVien::find($id);
        return view('admin.tinhtrangnghien.add',compact('hocvien'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['hoc_vien_id'] = $request->hoc_vien_id;
        $data['cac_benh_kem_theo'] = $request->cac_benh_kem_theo;
        $data['thuong_xuyen_su_dung'] = $request->thuong_xuyen_su_dung;
        $data['co_dia_di_ung'] = $request->co_dia_di_ung;
        $data['gia_dinh_ai_nghien'] = $request->gia_dinh_ai_nghien;
   
        $result = $this->TinhTrangNghienService->store($data);
        DB::table('qua_trinh_su_dung')->insert([
            'tinh_trang_nghien_id'=>$result->id,
            'lan_dau'=>$request->lan_dau,
            'ly_do'=>$request->ly_do,
            'su_dung_hang_ngay'=>$request->su_dung_hang_ngay,
            'ngay_may_lan'=>$request->ngay_may_lan,
            'ham_luong_su_dung'=>$request->ham_luong_su_dung,
            'khong_su_dung'=>$request->khong_su_dung,
            'da_dung_loai_nao'=>$request->da_dung_loai_nao,
            'hinh_thuc_su_dung'=>$request->hinh_thuc_su_dung,
            'su_dung_gan_nhat'=>$request->su_dung_gan_nhat,
        ]);
        DB::table('so_lan_cai_nghien')->insert([
            'tinh_trang_nghien_id'=>$result->id,
            'lan_cai_nghien'=>$request->lan_cai_nghien,
            'lan_thu_nhat'=>$request->lan_thu_nhat,
            'thoi_gian_lan_thu_nhat'=>$request->thoi_gian_lan_thu_nhat,
            'phuong_phap_lan_thu_nhat'=>$request->phuong_phap_lan_thu_nhat,
            'ly_do_tai_nghien_lan_thu_nhat'=>$request->ly_do_tai_nghien_lan_thu_nhat,
            'lan_thu_hai'=>$request->lan_thu_hai,
            'thoi_gian_lan_thu_hai'=>$request->thoi_gian_lan_thu_hai,
            'phuong_phap_lan_thu_hai'=>$request->phuong_phap_lan_thu_hai,
            'ly_do_tai_nghien_lan_thu_hai'=>$request->ly_do_tai_nghien_lan_thu_hai,
        ]);
        
        return redirect()->route('admin.tinhtrangnghien.index')->with('message', 'Đánh giá tình trạng nghiện thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hocvien = HocVien::find($id);
        $tinhtrangnghiens = DB::table('tinh_trang_nghien')
        ->Leftjoin('hoc_viens', 'tinh_trang_nghien.hoc_vien_id', '=', 'hoc_viens.id')
        ->Leftjoin('so_lan_cai_nghien', 'tinh_trang_nghien.id', '=', 'so_lan_cai_nghien.tinh_trang_nghien_id')
        ->Leftjoin('qua_trinh_su_dung', 'tinh_trang_nghien.id', '=', 'qua_trinh_su_dung.tinh_trang_nghien_id')
        ->select('hoc_viens.ten', 'tinh_trang_nghien.*','so_lan_cai_nghien.*','qua_trinh_su_dung.*')
        ->where('tinh_trang_nghien.hoc_vien_id',$hocvien->id)
        ->get();
        return view('admin.tinhtrangnghien.detail',compact('tinhtrangnghiens','hocvien'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hocvien = HocVien::find($id);
        $tinhtrangnghiens = DB::table('tinh_trang_nghien')
        ->Leftjoin('hoc_viens', 'tinh_trang_nghien.hoc_vien_id', '=', 'hoc_viens.id')
        ->Leftjoin('so_lan_cai_nghien', 'tinh_trang_nghien.id', '=', 'so_lan_cai_nghien.tinh_trang_nghien_id')
        ->Leftjoin('qua_trinh_su_dung', 'tinh_trang_nghien.id', '=', 'qua_trinh_su_dung.tinh_trang_nghien_id')
        ->select('hoc_viens.ten', 'tinh_trang_nghien.*','so_lan_cai_nghien.*','qua_trinh_su_dung.*')
        ->where('tinh_trang_nghien.hoc_vien_id',$hocvien->id)
        ->get();
        return view('admin.tinhtrangnghien.edit',compact('tinhtrangnghiens','hocvien'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $result = $this->TinhTrangNghienService->update($request->all(),$request->tinh_trang_nghien_id);
        DB::table('qua_trinh_su_dung')
              ->where('tinh_trang_nghien_id', $request->tinh_trang_nghien_id)
              ->update([
                'tinh_trang_nghien_id'=>$request->tinh_trang_nghien_id,
                'lan_dau'=>$request->lan_dau,
                'ly_do'=>$request->ly_do,
                'su_dung_hang_ngay'=>$request->su_dung_hang_ngay,
                'ngay_may_lan'=>$request->ngay_may_lan,
                'ham_luong_su_dung'=>$request->ham_luong_su_dung,
                'khong_su_dung'=>$request->khong_su_dung,
                'da_dung_loai_nao'=>$request->da_dung_loai_nao,
                'hinh_thuc_su_dung'=>$request->hinh_thuc_su_dung,
                'su_dung_gan_nhat'=>$request->su_dung_gan_nhat,
            ]);
        DB::table('so_lan_cai_nghien')
            ->where('tinh_trang_nghien_id', $request->tinh_trang_nghien_id)
            ->update([
                'tinh_trang_nghien_id'=>$request->tinh_trang_nghien_id,
                'lan_cai_nghien'=>$request->lan_cai_nghien,
                'lan_thu_nhat'=>$request->lan_thu_nhat,
                'thoi_gian_lan_thu_nhat'=>$request->thoi_gian_lan_thu_nhat,
                'phuong_phap_lan_thu_nhat'=>$request->phuong_phap_lan_thu_nhat,
                'ly_do_tai_nghien_lan_thu_nhat'=>$request->ly_do_tai_nghien_lan_thu_nhat,
                'lan_thu_hai'=>$request->lan_thu_hai,
                'thoi_gian_lan_thu_hai'=>$request->thoi_gian_lan_thu_hai,
                'phuong_phap_lan_thu_hai'=>$request->phuong_phap_lan_thu_hai,
                'ly_do_tai_nghien_lan_thu_hai'=>$request->ly_do_tai_nghien_lan_thu_hai,
        ]);

      
        return redirect()->route('admin.tinhtrangnghien.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
