<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\TaiChinhQuySheet;
use Carbon\Carbon;

class TaiChinhQuyExport implements WithMultipleSheets
{
    use Exportable;
    protected $hocvien;
    protected $nam;

    function __construct($hocvien, $nam)
    {
        $this->hocvien = $hocvien;
        $this->nam = $nam;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function sheets(): array
    {
        $sheets = [];
        for ($i = 1; $i <= 4; $i++) {
            $quy = Carbon::create()->month($i)->format('m');
            $sheets[] = new TaiChinhQuySheet($this->hocvien, $quy, $this->nam);
        }
        return $sheets;
    }

}
