<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TaiChinhNamExport;
use App\Exports\TaiChinhQuyExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DienGiaiTaiChinh;
use App\Models\TaiChinh;
use App\Models\HocVien;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\TaiChinhExport;
use DB;

class TaiChinhController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $hocvien = HocVien::find($request->hoc_vien_id);
        $diengiai_thu = DienGiaiTaiChinh::where('thu_chi',0)->get();
        $diengiai_chi = DienGiaiTaiChinh::where('thu_chi',1)->get();
        if($hocvien->taichinh()->get()->count() >0){
            $hocvien['taichinh'] = 1;
        }
    
       
        return view('admin.student.form',
            compact('diengiai_thu',
             'diengiai_chi',
                'hocvien'
            )
        );
    }

    public function filterMonth(Request $request,$id){
        $hocvien = HocVien::find($id);

        $month = new \Carbon\Carbon($request->month); 
        $days_month = $month->daysInMonth;
        $end_month = $month->endOfMonth();
        $first_month = $month->firstOfMonth();
        $ngay_nghi = 0;
        if($hocvien->vephep()->where('thang_ve',$month->format('m'))->whereNotNull('thang_vao')->get()->count()>0){
            if($hocvien->vephep()->where('thang_ve',$month->format('m'))->where('thang_vao',$month->format('m'))->get()->count()){
                foreach ($hocvien->vephep()->where('thang_ve',$month->format('m'))->where('thang_vao',$month->format('m'))->get() as $value) {
                    $ngay_ve =  new \Carbon\Carbon($value->ngay_ve);
                    $ngay_vao =  new \Carbon\Carbon($value->ngay_vao); 
                    $ngay_nghi += $ngay_ve->diff($ngay_vao)->days;
                }
            }elseif($hocvien->vephep()->where('thang_ve',$month->format('m'))->get()->count()){
                foreach ($hocvien->vephep()->where('thang_ve',$month->format('m'))->get() as $value) {
                    $ngay_ve =  new \Carbon\Carbon($value->ngay_ve);
                    $ngay_nghi += $ngay_ve->diff($end_month)->days;
                }
            }elseif($hocvien->vephep()->where('thang_vao',$month->format('m'))->get()->count()){
                foreach ($hocvien->vephep()->where('thang_vao',$month->format('m'))->get() as $value) {
                    $ngay_vao =  new \Carbon\Carbon($value->ngay_ve);
                    $ngay_nghi += $ngay_vao->diff($first_month)->days;
                }
            }
        }
        $taichinh = $hocvien->taichinh()->where('thang',$month->format('m'))->first();
        if($taichinh !== NULL){
            $taichinh = DB::table('thu_chi_tai_chinhs')
            ->Leftjoin('tai_chinhs', 'thu_chi_tai_chinhs.tai_chinh_id', '=', 'tai_chinhs.id')
            ->Leftjoin('dien_giai_tai_chinhs', 'thu_chi_tai_chinhs.dien_giai_tai_chinh_id', '=', 'dien_giai_tai_chinhs.id')
            ->select('thu_chi_tai_chinhs.*', 'tai_chinhs.tong_ngay_ngi', 'tai_chinhs.tong_ngay_co_mat','tai_chinhs.thang','dien_giai_tai_chinhs.ten_dien_giai','dien_giai_tai_chinhs.thu_chi')
            ->where('thu_chi_tai_chinhs.tai_chinh_id',$taichinh->id)
            ->get();
        }
        $ngay_co_mat = $days_month - $ngay_nghi;
        return response()->json(['taichinh'=>$taichinh,'ngay_co_mat'=>$ngay_co_mat,'ngay_nghi'=>$ngay_nghi]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $so_tien_arr = $request->so_tien;
        $tmp_year =$request->thang . '-01';
        $taichinh = TaiChinh::create([
            'hoc_vien_id'=>$request->hoc_vien_id,
            'tong_ngay_ngi'=>$request->tong_ngay_ngi,
            'tong_ngay_co_mat'=>$request->tong_ngay_co_mat,
            'thang' => substr($request->thang, -2),
        ]);
        foreach ($request->dien_giai_tai_chinh as $key) {
            $diengiai = DienGiaiTaiChinh::find($key);
            $so_tien = array_shift($so_tien_arr);
            if($diengiai->thu_chi == 0)
                $thu = (int)str_replace(",","",$so_tien);
            else
                $thu = null;
            if($diengiai->thu_chi == 1)
                $chi = (int)str_replace(",","",$so_tien);
            else
                $chi = null;
            DB::table('thu_chi_tai_chinhs')->insert([
                'tai_chinh_id' => $taichinh->id,
                 'dien_giai_tai_chinh_id' => $key,
                 'thu' => $thu,
                 'chi' => $chi,
                 'created_at' => $tmp_year
                 ]
            );
        }
  
        
        return redirect()->route('admin.hoc-vien.show',$request->hoc_vien_id)->with('message', 'thêm tài chính thành công');
    }

    public function export(Request $request, $id){
        $hocvien = HocVien::find($id);
        $option_export = $request->option_export;
        $nam = $request->year_export;

        switch ($option_export) {
            case 'thang':
                return Excel::download(new TaiChinhExport($hocvien, $nam), 'Tai_chinh_HV_'.$hocvien->ten.'.xlsx');
                break;

            case 'quy':
                return Excel::download(new TaiChinhQuyExport($hocvien, $nam), 'Tai_chinh_quy_HV_'.$hocvien->ten.'.xlsx');
                break;

            case 'nam':
                return Excel::download(new TaiChinhNamExport($hocvien, $nam), 'Tai_chinh_nam_HV_'.$hocvien->ten.'.xlsx');
                break;

            default:
                return redirect()->route('admin.hoc-vien.show',$request->hoc_vien_id)->with('message', 'Xuất không thành công');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
