<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrangThaiHocTap extends Model
{
    protected $table = 'trang_thai_hoc_tap';

    protected $fillable = [
        'id',
        'ten'
    ];

    public function hocVien()
    {
        return $this->hasMany(HocVien::class,'id');
    }
}
