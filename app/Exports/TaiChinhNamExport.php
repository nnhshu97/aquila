<?php

namespace App\Exports;

use App\Exports\Sheets\TaiChinhNamSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class TaiChinhNamExport implements WithMultipleSheets
{
    use Exportable;
    protected $hocvien;
    protected $nam;

    function __construct($hocvien, $nam)
    {
        $this->hocvien = $hocvien;
        $this->nam = $nam;
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function sheets(): array
    {
        $sheets = [];
        $sheets[] = new TaiChinhNamSheet($this->hocvien, $this->nam);
        return $sheets;
    }

}
