<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrangThaiHocTapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trang_thai_hoc_tap')->delete();
        $lydo_diem =[
            [
                'ten' => "Đang học tập",
            ],
            [
                'ten' => "Đã tốt nghiệp",
            ]
      ];
      DB::table('trang_thai_hoc_tap')->insert($lydo_diem);
    }
}
