@extends('admin.layouts.index')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h4>Danh sách khiếu nại của gia đình học viên</h4>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"> Khiếu nại</li>
                    </ol>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content-header -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <table id="report" class="table table-bordered"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Người bảo hộ</th>
                                    <th>Học viên</th>
                                    <th>Vấn đề</th>
                                    <th>Nội dung</th>
                                    <th>Hành động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reports as $key => $report)
                                    <tr role="row" class="odd">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $report->nguoibaoho->ten }}</td>
                                        <td>
                                            {{ \App\Models\HocVien::where(['nguoi_bao_ho_id' => $report->nguoibaoho_id])->pluck('ten')->first() }}
                                        </td>
                                        <td>
                                            @foreach(config('admin.category_report') as $key => $value)
                                                @if($report->category_report == $value['val'])
                                                    {{ $value['text']}}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ substr($report->content, 0, 50) . '...' }}</td>
                                        <td>
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown show">
                                                    <a class="btn btn-info dropdown-toggle dropdown-icon"
                                                       data-toggle="dropdown" href="#" aria-expanded="true">
                                                        Tác vụ
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                                        <a href="{{ route('admin.report.show', $report->id ) }}"
                                                           title="Xem chi tiết"
                                                           class="dropdown-item">
                                                            <i class="fas fa-info-circle"></i> Chi tiết
                                                        </a>
                                                        <a href="javascript:void(0);" title="Xoá"
                                                           class="dropdown-item delete-trigger delete"
                                                           data-toggle="modal"
                                                           data-id='{{ $report->id }}'
                                                           data-target="#modal-danger">
                                                            <i class="fas fa-trash"></i> Xoá
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{ Form::open(['route' => ['admin.report.destroy', 'ID_REPLY_IN_URL'], 'id' => 'delete-report-form', 'class' => 'form-horizontal']) }}
        {{ method_field('DELETE') }}
    {{ Form::close() }}

    <div class="mess-data" data-url-delete="{{ route('admin.report.destroy', 'ID_REPLY_IN_URL') }}">
    </div>

    @component('admin.components.modals.alert-delete')
        @slot('headerText')
            {{ "Bạn có muốn xóa report" }}
        @endslot
    @endcomponent

@endsection
@section('scripts')
    <script type="text/javascript">
        $('#report').DataTable({
            "language": {
                "url": "/admin_assets/plugins/datatables-bs4/lang/vietnamese-lang.json"
            },
            'columnDefs': [{
                'targets': 5,
                'sortable': false
            }],
            "paging": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "responsive": true,
        });

        var elementData = $('.mess-data');
        var urlDelete = elementData.data('url-delete');

        // submit del
        $('body').on('click', '.delete-trigger', function (event) {
            event.preventDefault();
            $('#modal-danger').modal('show', {backdrop: 'true'});
            let action = urlDelete.replace('ID_REPLY_IN_URL', $(this).data('id'));
            console.log(action);
            $('#delete-report-form').attr('action', action);
        });
        // confirm del
        $('body').on('click', '#modal-danger .yes-confirm', e => {
            $('#delete-report-form').submit();
        });

    </script>
@endsection



