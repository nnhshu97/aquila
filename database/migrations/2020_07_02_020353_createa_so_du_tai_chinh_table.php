<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateaSoDuTaiChinhTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_du_tai_chinh', function (Blueprint $table) {
        $table->integer('hoc_vien_id')->unsigned();
        $table->integer('tai_chinh_id')->unsigned();
        $table->foreign('hoc_vien_id')->references('id')->on('hoc_viens')->onDelete('cascade');
        $table->foreign('tai_chinh_id')->references('id')->on('tai_chinhs')->onDelete('cascade');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_du_tai_chinh');
    }
}
