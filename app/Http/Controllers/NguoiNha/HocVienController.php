<?php

namespace App\Http\Controllers\NguoiNha;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\Admin\HocVienService;
use DB;
use App\Models\LyDoDiem;
use App\Models\LopHoc;
use App\Models\TaiChinh;
use App\Services\Admin\KhamSucKhoeService;


class HocVienController extends Controller
{
    public function __construct(HocVienService $hocvienService,KhamSucKhoeService $khamsuckhoeService)
    {
        $this->hocvienService = $hocvienService;
        $this->khamsuckhoeService = $khamsuckhoeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('nguoinha.hocvien.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function diemDanh()
    {
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien;
        $lophocs = $hocvien->lophoc;
        return view('nguoinha.hocvien.diemdanh',compact('lophocs','hocvien'));
    }
    public function sucKhoe()
    {
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien->id;
        $khamsuckhoes = DB::table('kham_suc_khoes')->where('hoc_vien_id',$hocvien)->get();
        $khamsuckhoe = $this->khamsuckhoeService->show($khamsuckhoes[ $khamsuckhoes->count()-1]->id);
        return view('nguoinha.hocvien.suckhoe',compact('khamsuckhoes','khamsuckhoe'));
    }

    public function sucKhoeDetail(Request $request){
         $khamsuckhoes = $this->khamsuckhoeService->show($request->id);
        return response()->json($khamsuckhoes);
    }

    public function hocPhi()
    {
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien;
        $hv_taichinh = $hocvien->taichinh;
        $taichinhs = null;
        if(count($hv_taichinh) !== 0){
            foreach ($hv_taichinh as $key) {
                $taichinh_id = $key->id;
                $hocvien['tong_ngay_nghi'] = $key->tong_ngay_ngi;
                $hocvien['tong_ngay_co_mat'] = $key->tong_ngay_co_mat;
            }
            $taichinhs = DB::table('thu_chi_tai_chinhs')
            ->Leftjoin('tai_chinhs', 'thu_chi_tai_chinhs.tai_chinh_id', '=', 'tai_chinhs.id')
            ->Leftjoin('dien_giai_tai_chinhs', 'thu_chi_tai_chinhs.dien_giai_tai_chinh_id', '=', 'dien_giai_tai_chinhs.id')
            ->select('thu_chi_tai_chinhs.*', 'tai_chinhs.tong_ngay_ngi','tai_chinhs.thang', 'tai_chinhs.tong_ngay_co_mat','dien_giai_tai_chinhs.ten_dien_giai','dien_giai_tai_chinhs.thu_chi')
            ->where('tai_chinhs.id',$taichinh_id)
            ->get();
          
        }
        return view('nguoinha.hocvien.hocphi', compact('taichinhs','hocvien'));
    }

    public function filterHocPhi($id,Request $request){
        $request->request->add(['id' => $id]);
        $taichinh = $this->hocvienService->filterTaiChinh($request);
        if($taichinh !== false){
            return response()->json(['message'=>$taichinh],200);
        }
        return response()->json(['message'=>$taichinh],422);
    }
    public function diemSo()
    {  
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien;
        $lophocs = $hocvien->lophoc;

        return view('nguoinha.hocvien.diemhocvien', compact('lophocs'));
    }
    public function filterDiemSo(Request $request){

        $lydodiem = LyDoDiem::all();
        $lophoc = LopHoc::find($request->id);
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien->id;
        $hocviens = LopHoc::find($request->id)->hocvien;
        $diem_monhoc = DB::table('diem_mon_hocs')
        ->Leftjoin('hoc_viens', 'diem_mon_hocs.hoc_vien_id', '=', 'hoc_viens.id')
        ->select('hoc_viens.id', 'diem_mon_hocs.diem','diem_mon_hocs.ly_do_id','diem_mon_hocs.created_at')
        ->where('diem_mon_hocs.lop_hoc_id',$request->id)
        ->where('diem_mon_hocs.hoc_vien_id',$hocvien)
        ->get();
        $diem_hv = [];
            foreach($hocviens as $hocvien )
            {
                foreach($diem_monhoc as $diem) {
                    if($diem->id == $hocvien->id){
                       foreach ($lydodiem as $lydo) {
                          if($lydo->id == $diem->ly_do_id){
                            $diem_hv[] = ['ngay'=>date('d-m-Y', strtotime($diem->created_at)),'ly_do'=>$lydo->ten,'diem'=>$diem->diem];
                          }

                       }
                     
                    }
                }
            }
        return response()->json($diem_hv);
    }

    public function filterDiemDanh(Request $request){
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien;
        $lophoc = LopHoc::find($request->id);
       
        $diemdanh = $hocvien->diemdanh()->wherePivot('lop_hoc_id',$lophoc->id)->get();
        $thoikhoabieu = DB::table('thoi_khoa_bieus')
        ->Leftjoin('lop_hocs', 'thoi_khoa_bieus.lop_hoc_id', '=', 'lop_hocs.id')
        ->select('thoi_khoa_bieus.*', 'lop_hocs.ma_lop_hoc')
        ->where('lop_hocs.id',$lophoc->id)
        ->get();
        $diemdanhHocVien = [];
        $count = 0;
        foreach($thoikhoabieu as $thoi_khoa_bieu){
           foreach($diemdanh as $diem_danh){
               if(date("Y-m-d",strtotime($thoi_khoa_bieu->ngay)) == date("Y-m-d",strtotime($diem_danh->pivot->ngay))){
                   if($thoi_khoa_bieu->sang != NULL && $diem_danh->pivot->ca_hoc == 0){
                        $diemdanhHocVien[] = [
                            'ngay'=>date("d-m-Y",strtotime($thoi_khoa_bieu->ngay)),
                            'ca_hoc'=>$thoi_khoa_bieu->sang,
                            'diem_danh'=>1
                        ];
                   }if($thoi_khoa_bieu->chieu != NULL && $diem_danh->pivot->ca_hoc == 1){
                        $diemdanhHocVien[] = [
                            'ngay'=>date("d-m-Y",strtotime($thoi_khoa_bieu->ngay)),
                            'ca_hoc'=>$thoi_khoa_bieu->chieu,
                            'diem_danh'=>1
                        ];
                   }
               }
           }
           if($thoi_khoa_bieu->sang != NULL){
                $count++;
           }if($thoi_khoa_bieu->chieu != NULL){
                $count++;
           }
        }
        $lophoc['sobuoi'] = count($diemdanh).'/'.$count;
       return response()->json(['message'=>$diemdanhHocVien,'lophoc'=>$lophoc]);

    }

    public function tinhTrangNghien(Request $request){
        $nguoibaoho = Auth::guard('nguoibaoho')->user();
        $hocvien = $nguoibaoho->hocvien->id;
        $tinhtrangnghiens = DB::table('tinh_trang_nghien')
        ->Leftjoin('hoc_viens', 'tinh_trang_nghien.hoc_vien_id', '=', 'hoc_viens.id')
        ->Leftjoin('so_lan_cai_nghien', 'tinh_trang_nghien.id', '=', 'so_lan_cai_nghien.tinh_trang_nghien_id')
        ->Leftjoin('qua_trinh_su_dung', 'tinh_trang_nghien.id', '=', 'qua_trinh_su_dung.tinh_trang_nghien_id')
        ->select('hoc_viens.ten', 'tinh_trang_nghien.*','so_lan_cai_nghien.*','qua_trinh_su_dung.*')
        ->where('tinh_trang_nghien.hoc_vien_id',$hocvien->id)
        ->get();
        return response()->json(['message'=>$tinhtrangnghiens]);
    }
}
