<?php

namespace App\Services\Admin;

use App\Models\HocVien;
use App\Models\NguoiBaoHo;
use App\Models\AnhHocVien;
use App\Services\Helpers\ImageService;
use Exception;
use DB;
use Auth;
use App\Models\TaiChinh;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class HocVienService extends BaseService
{
    public function model()
    {
        return HocVien::class;
    }

    public function getListHocVien($conditions)
    {
        // $keyword = escape_like($conditions['search']['value']);
        // $keyword = "";
        // $searchColumns = ['name'];
        // $limit = $conditions['length'];
        // $page = $conditions['start'] / $conditions['length'] + 1;
        // $orderConditions = [];
        // if (!empty($conditions['order'])) {
        //     $orderParams = $conditions['order'];
        //     $orderConditions['column'] = $conditions['columns'][$orderParams[0]['column']]['data'];
        //     $orderConditions['dir'] = $orderParams[0]['dir'];
        // }

        // $query = empty($conditions['order']) ? $this->model->orderBy('id', 'desc') : $this->model;
        // $results = self::search($query, $keyword, $searchColumns, $orderConditions, $limit, $page);

        // return $returnData = [
        //     'recordsFiltered' => $results->total(),
        //     'data' => $results->items()
        // ];

        if(Auth::guard('admin')->user()->role == 1){
            $hocVienList = $this->model->with('nguoibaoho')->with('trangThaiHocTap')->get();
        }else{
            if(Auth::guard('admin')->user()->gender == 1 ){
                $hocVienList = $this->model->where('gioi_tinh','Nam')->with('nguoibaoho')->with('trangThaiHocTap')->get();
            }else{
                $hocVienList = $this->model->where('gioi_tinh','Nữ')->with('nguoibaoho')->with('trangThaiHocTap')->get();
            }
        }
        return response()->json([
            'data' => $hocVienList
        ]);
    }

    public function store($request)
    {

        try {

            DB::beginTransaction();

            $dataNguoiBaoHo = [
                'ten' => $request['ho_va_ten_nguoi_bao_ho'],
                'email' => $request['email'],
                'password' => Hash::make(config('hocvien.nguoi_bao_ho.password_default')),
                'quan_he_hoc_vien' => $request['quan_he_hoc_vien'],
                'so_dien_thoai' => $request['so_dien_thoai']
            ];

            $dataHocVien = array_except($request, [
                'ho_va_ten_nguoi_bao_ho',
                'email_nguoi_bao_ho',
                'quan_he_hoc_vien',
                'so_dien_thoai',
            ]);

            $nguoiBaoHo = NguoiBaoHo::create($dataNguoiBaoHo);

            if ($nguoiBaoHo) {
                $stt = $nguoiBaoHo->id;
                $ngay_vao = date("m.Y",strtotime($dataHocVien['ngay_vao']));
                $dataHocVien['nguoi_bao_ho_id'] = $nguoiBaoHo->id;
                $dataHocVien['ma_hoc_vien'] = $stt.'-'.$ngay_vao;
            }
            $hocVien = $this->model->create($dataHocVien);

            if (isset($dataHocVien['avatar_hocvien'])) {
                $type = 'user_avatar';
                $dataHocVien['avatar_hocvien'] = ImageService::uploadFile($dataHocVien['avatar_hocvien'], $type, config('images.paths.' . $type));
                $hocVien->avatar_url = $dataHocVien['avatar_hocvien'];
                $hocVien->save();
            }

            if (isset($dataHocVien['images'])) {
                $typeImages = 'image_hocvien';
                foreach ($dataHocVien['images'] as $image) {
                    $fileName = ImageService::uploadFile($image, $typeImages, config('images.paths.' . $typeImages));
                    $hocVien->anhhocvien()->create(['anh_hoc_vien' => $fileName]);
                }
            }

            DB::commit();

            return $hocVien;
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e);

            return false;
        }
    }

    public function edit($request, $id)
    {

        try {
            DB::beginTransaction();
//             dd($request);
            $hocVien = $this->findOrFail($id);
            $imageHocVien = $hocVien->anhhocvien()->get();
            $avatarUrl = $hocVien->getOriginal('avatar_url');

            $dataNguoiBaoHo = [
                'ten' => $request['ho_va_ten_nguoi_bao_ho'],
                'email' => $request['email'],
                'password' => Hash::make(config('hocvien.nguoi_bao_ho.password_default')),
                'quan_he_hoc_vien' => $request['quan_he_hoc_vien'],
                'so_dien_thoai' => $request['so_dien_thoai']
            ];

            $dataHocVien = array_except($request, [
                'ho_va_ten_nguoi_bao_ho',
                'email',
                'quan_he_hoc_vien',
                'so_dien_thoai',
                'avatar_hocvien'
            ]);

            $nguoiBaoHo = $hocVien->nguoibaoho();

            $nguoiBaoHo->update($dataNguoiBaoHo);

            $hocVien->update($dataHocVien);

            if (isset($request['avatar_hocvien'])) {
                $type = 'user_avatar';
                $dataHocVien['avatar_hocvien'] = ImageService::uploadFile($request['avatar_hocvien'], $type, config('images.paths.' . $type));
                $hocVien->avatar_url = $dataHocVien['avatar_hocvien'];
                $hocVien->save();

                if ($avatarUrl) {
                    foreach (config('images.dimensions.user_avatar') as $size => $value) {
                        $fileName = ($size == 'original' ? 'original' : $size ) . '/' . $avatarUrl;

                        if ($fileName) {
                            ImageService::delete(config('images.paths.' . $type), $fileName);
                        }
                    }
                }
            }

            $typeImages = 'image_hocvien';

            if (isset($dataHocVien['images'])) {
                foreach ($dataHocVien['images'] as $image) {
                    $fileName = ImageService::uploadFile($image, $typeImages, config('images.paths.' . $typeImages));
                    $hocVien->anhhocvien()->create(['anh_hoc_vien' => $fileName]);
                }
            }

            if (count($imageHocVien) && isset($request['isDeleteImages'])) {
                foreach ($imageHocVien as $img) {
                    foreach (config('images.dimensions.image_hocvien') as $size => $value) {
                        $fileNameImage = ($size == 'original' ? 'original' : $size ) . '/' . $img->anh_hoc_vien;

                        ImageService::delete(config('images.paths.' . $typeImages), $fileNameImage);
                    }
                    $img->delete();
                }
            }

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            Log::debug($e);

            return false;
        }
    }

    public function filterTaiChinh($request){

        $month = date("m",strtotime($request->month));
        $id_tai_chinh = TaiChinh::where('tai_chinhs.thang',$month)->first();
        if($id_tai_chinh !== null){
            $taichinh = DB::table('thu_chi_tai_chinhs')
            ->Leftjoin('tai_chinhs', 'thu_chi_tai_chinhs.tai_chinh_id', '=', 'tai_chinhs.id')
            ->Leftjoin('dien_giai_tai_chinhs', 'thu_chi_tai_chinhs.dien_giai_tai_chinh_id', '=', 'dien_giai_tai_chinhs.id')
            ->select('thu_chi_tai_chinhs.*', 'tai_chinhs.tong_ngay_ngi', 'tai_chinhs.tong_ngay_co_mat','tai_chinhs.thang','dien_giai_tai_chinhs.ten_dien_giai','dien_giai_tai_chinhs.thu_chi')
            ->where('thu_chi_tai_chinhs.tai_chinh_id',$id_tai_chinh->id)
            ->where('tai_chinhs.hoc_vien_id',$request->id)
            ->get();
            return $taichinh;
        }
        return false;

    }

    public function addImageQuaTrinh($request){
        $hocvien = $this->findOrFail($request['id']);
        if (isset($request['images'])) {
            $typeImages = 'image_hocvien';
            foreach ($request['images'] as $image) {
                $fileName = ImageService::uploadFile($image, $typeImages, config('images.paths.' . $typeImages));
                $hocvien->anhquatrinh()->create(['anh_hoc_vien' => $fileName]);
            }
            return true;
        }
        return false;
    }

    public function deleteHocVien($id)
    {
        try {
            $hocVien = $this->findOrFail($id);
            $hocVien->delete();
            return true;
        } catch (Exception $e) {
            Log::debug($e);

            return false;
        }
    }


}
