<meta charset="utf-8">
<meta name="description" content="Trung tâm cai nghiện Aquila">
<meta name="keywords" content="aquila, aquila center, trung tâm cai nghiện">
<meta name="author" content="Aquila">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Aquila | Quản lí trung tâm</title>
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" type="image/png" href=""/>
{{ Html::style('admin_assets/dist/css/adminlte.css') }}
<!-- daterange picker -->
{{ Html::style('admin_assets/plugins/daterangepicker/daterangepicker.css') }}
<!-- Tempusdominus Bbootstrap 4 -->
{{ Html::style('admin_assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}
<!-- color-picker -->
{{ Html::style('admin_assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}
<!-- date time-picker -->
{{ Html::style('admin_assets/plugins/bootstrap-datepicker/bootstrap-datepicker.css') }}
<!-- i check bootstrap -->
{{ Html::style('admin_assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}
<!-- Overlay scroll bar -->
{{ Html::style('admin_assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}

<!-- {{ Html::style('admin_assets/plugins/summernote/dist/summernote-bs4.css') }} -->
{{ Html::style('admin_assets/plugins/sweetalert2/sweetalert2.min.css') }}
{{ Html::style('admin_assets/plugins/toastr/toastr.min.css') }}

<!-- DataTables -->
{{ Html::style('admin_assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}
{{ Html::style('admin_assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}
{{ Html::style('admin_assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}
<!-- Cdn load -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.10/css/dataTables.checkboxes.css">
<!-- fullCalendar -->
{{ Html::style('admin_assets/plugins/fullcalendar/main.min.css') }}
{{ Html::style('admin_assets/plugins/fullcalendar-daygrid/main.min.css') }}
{{ Html::style('admin_assets/plugins/fullcalendar-timegrid/main.min.css') }}
{{ Html::style('admin_assets/plugins/fullcalendar-bootstrap/main.min.css') }}
<!-- admin -->
{{ Html::style('admin_assets/dist/css/admin.css') }}
@yield('styles')
